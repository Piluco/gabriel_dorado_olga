import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../header/header.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';



@NgModule({
  declarations: [HeaderComponent, EncabezadoComponent],
  imports: [
    CommonModule
  ],
  exports: [HeaderComponent, EncabezadoComponent]
})
export class HeaderModule { }
