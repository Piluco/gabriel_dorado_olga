export class Contacto {
    id!: number;
    nombre!: string;
    apellido!: string;
    dni!: number;
    edad!: string;
    estadocivil!: string;
    email!: string;
    direccion!: string;
    provincia!: string;
    pais!: string;
    celular!: string;
    contactosec!: string;
    yearcursado!: string;
    modalidad!: string;
    fecha!: string;
    institucion!: string;
    
}