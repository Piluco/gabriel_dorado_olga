import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Contacto } from '../model/contacto';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor() {
    this.CargarDatosForm();
   }
  opciones: string[] = [];
  contactForm: any = FormGroup;
  contacto: Contacto = new Contacto();
  emailPattern: any = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  listaEstadoCivil = ['Seleccione','Soltero/a','Casado/a','Divorciado/a','Viudo/a'];
  listaPais = ['Seleccione','Argentina','Chile','Paraguay','Uruguay','Bolivia','Brazil'];
  listaCheck = ['Constancia de Alumno Regular','Constancia de 7ºmo grado','Padre o Madre de Hijo/s','Partida de Nacimiento','Copia de Documento'];
  listaYear = ['Seleccione','Primer Año','Segundo Año','Tercer Año'];
  listaEija = ['Seleccione','Primer Año','Segundo Año'];
  listaSeija = ['Seleccione','Segundo Año'];
  displayedColums = ['id','nombre','apellido','dni','edad','estadocivil','email','direccion','provincia','pais','celular'];
  dataSource!: MatTableDataSource<Contacto>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  CargarDatosForm(): void {
    this.opciones= [];
    this.contacto = new Contacto();
    this.contactForm = this.CrearFormGroup();
  }

  CrearFormGroup(): FormGroup {
    return new FormGroup({
      nombre: new FormControl('',[Validators.required,Validators.minLength(10)]),
      apellido: new FormControl('',[Validators.required,Validators.minLength(4)]),
      dni: new FormControl('',[Validators.required,Validators.minLength(8),Validators.maxLength(8),Validators.min(10000000),Validators.max(50000000)]),
      edad: new FormControl('',[Validators.required,Validators.minLength(2),Validators.maxLength(3),Validators.min(15)]),
      estadocivil: new FormControl('seleccione',[Validators.required,Validators.minLength(5)]),
      email: new FormControl('',[Validators.required,Validators.minLength(8),Validators.pattern(this.emailPattern)]),
      direccion: new FormControl('',[Validators.required,Validators.minLength(30)]),
      provincia: new FormControl('',[Validators.required,Validators.minLength(5),Validators.maxLength(15)]),
      pais: new FormControl('seleccione',[Validators.required,Validators.minLength(5)]),
      celular: new FormControl('',[Validators.required,Validators.minLength(7),Validators.maxLength(10)]),
      contactosec: new FormControl('',[Validators.required,Validators.minLength(7),Validators.maxLength(10)]),
      yearcursado: new FormControl('seleccione',[Validators.required,Validators.minLength(5)]),
      modalidad: new FormControl('seleccione',[Validators.required]),
    });
  }

  getClassInput(inputForm: FormControl): string{
    return !inputForm?.valid ? 'is-invalid' : 'is-valid'
  }

  getClassSelect(selectForm: FormControl): string{
    return !(selectForm.value !== 'seleccione' && selectForm?.valid) ? 'is-invalid' : 'is-valid'
  }

  verificar(c: string): void{
    let pos = this.opciones.indexOf(c);
    if (pos === -1 ){
      this.opciones.push(c);
    }else{
      this.opciones.splice(pos,1);
    }
  }

  cancelar(): void {
    this.CargarDatosForm();
  }

  validarSelect(estadocivil?: any, pais?: any, yearcursado?: FormControl): boolean {
    if (estadocivil && pais && yearcursado){
      return (estadocivil.value  !== 'seleccione' && pais.value !== 'seleccione' && yearcursado.value !== 'seleccione') ? true : false;
    }else{
      return (estadocivil.value !== 'seleccione' && pais.value !== 'seleccione') ? true : false;
    }
  }

  validarTodo(): boolean {
    return this.contactForm.valid ? true: false;
  }

  validarCheck(): boolean {
    return this.opciones.length>0 ? true: false;
  }

  lista: Contacto[] = [];
  guardar(): void{
    this.contacto = this.contactForm.value;
    console.log('objeto:', this.contacto);
    this.lista.push(this.contacto);
    console.log('lista:', this.lista);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.lista);
    this.dataSource.paginator = this.paginator;
  }
get nombre(){return this.contactForm.get('nombre');}
get apellido(){return this.contactForm.get('apellido');}
get dni(){return this.contactForm.get('dni');}
get edad(){return this.contactForm.get('edad');}
get estadocivil(){return this.contactForm.get('estadocivil');}
get getemail(){return this.contactForm.get('email');}
get direccion(){return this.contactForm.get('direccion');}
get provincia(){return this.contactForm.get('provincia');}
get pais(){return this.contactForm.get('pais');}
get celular(){return this.contactForm.get('celular');}
get contactosec(){return this.contactForm.get('contactosec');}
get institucion(){return this.contactForm.get('institucion');}
get yearcursado(){return this.contactForm.get('yearcursado');}
get modalidad(){return this.contactForm.get('modalidad');}

}
