import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { PieDePaginaComponent } from './pie-de-pagina/pie-de-pagina.component';



@NgModule({
  declarations: [FooterComponent, PieDePaginaComponent],
  imports: [
    CommonModule
  ],
  exports: [FooterComponent, PieDePaginaComponent]
})
export class FooterModule { }
